package de.thk.parcer.parcer.services;


import de.thk.parcer.parcer.controllers.LabelController;
import de.thk.parcer.parcer.domain.Address;
import de.thk.parcer.parcer.domain.Customer;
import de.thk.parcer.parcer.domain.Label;
import de.thk.parcer.parcer.domain.Shipment;
import de.thk.parcer.parcer.exceptions.InvalidAddressException;
import de.thk.parcer.parcer.exceptions.ResourceNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
// Let each test method run in a transaction.
// Test methods in a transaction are rolled back by default.
@Transactional
public class LabelServiceImplTest {
    @Autowired
    private ShipmentService shipmentService;
    @Autowired
    private CustomerService customerService;

    private Customer testCustomer;

    @Autowired
    private LabelService labelService;

    private Label testLabel;

    private Address testAddress;

    private Shipment testShipment;

    private long testlabelID;

    @BeforeTransaction
    public void setUp() {

        Customer customer = new Customer();
        customer.setName("TH Koeln");
        testCustomer = customerService.createCustomer(customer);

        Address address = new Address();
        address.setLine1("René Wörzberger");
        address.setLine2("Claudiusstraße 16");
        address.setLine3("51503 Rösrath");
        testAddress = address;

        Shipment shipment = new Shipment();
        shipment.setCustomer(customer);
        shipment.setRecipientAddress(address);
        shipment.setRecipientAddress(testAddress);
        testShipment = shipmentService.createShipment(shipment, testCustomer.getId());

        Label label = new Label();
        label.setBase64EncodedLabel("TESTLABEL");

        testLabel = labelService.createLabel(address, shipment.getShipmentNumber());

        testlabelID = testLabel.getId();
    }

    @Test
    public void testCreateLabel() {
        //given
        Shipment shipment = new Shipment();
        shipment.setRecipientAddress(new Address());
        shipment.getRecipientAddress().setLine1("René Wörzberger");
        shipment.getRecipientAddress().setLine2("Claudiusstraße 16");
        shipment.getRecipientAddress().setLine3("51503 Rösrath");
        shipmentService.createShipment(shipment, testCustomer.getId());

        //when
        Label persistedLabel =  labelService.createLabel(testAddress, shipment.getShipmentNumber());

        //then
        assertNotNull("Label has been created with ID", persistedLabel.getId());
        assertNotNull("Label has been created with encoded Label  ", persistedLabel.getBase64EncodedLabel());
    }

    @Test
    public void testGetLabelByID() {
        //when
        Label gefundenesLabel = labelService.getLabelById(testlabelID);

        //then
        assertEquals(testLabel.getId(), gefundenesLabel.getId() );
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testGetLabelByFaultyID() {

        //when
        Label gefundenesLabel = labelService.getLabelById(-1);

        //then
        assertNotEquals(testLabel.getId(), gefundenesLabel.getId() );
    }
}